INCLUDEPATH += $${PWD}
DEPENDPATH += $${PWD}

QT += network

PUBLIC_HEADERS += \
    $${PWD}/QDnsDomainNameRecord \
    $${PWD}/QDnsHostAddressRecord \
    $${PWD}/QDnsLookup \
    $${PWD}/QDnsMailExchangeRecord \
    $${PWD}/QDnsServiceRecord \
    $${PWD}/QDnsTextRecord \
    $${PWD}/qdnslookup.h \
    $${PWD}/qdnslookup_p.h

# The following is not *really* private and needs to be installed (above).
# When backporting qdnslookup to Qt4, the private header needs to be included
# so that the MOC can resolve QDnsLookupPrivate and QDnsLookupReply when it
# implements the Q_PRIVATE_SLOT for _q_lookupFinished.
#PRIVATE_HEADERS += \
#    $${PWD}/qdnslookup_p.h

HEADERS *= \
    $${PUBLIC_HEADERS} \
    $${PRIVATE_HEADERS}

SOURCES += \
    $${PWD}/qdnslookup.cpp

android:{
    SOURCES += $${PWD}/qdnslookup_stub.cpp
}
else:symbian:{
    SOURCES += $${PWD}/qdnslookup_symbian.cpp
}
else:unix:{
    SOURCES += $${PWD}/qdnslookup_unix.cpp
}
else:win32: {
    SOURCES += $${PWD}/qdnslookup_win.cpp
    LIBS += -ldnsapi -lws2_32
}
else:{
    SOURCES += $${PWD}/qdnslookup_stub.cpp
}
