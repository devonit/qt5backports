include(qdnslookup-backport/qdnslookup-backport.pri)
include(qjson-backport/qjson-backport.pri)

QT -= gui

TARGET = qt5backports
TEMPLATE = lib
VERSION = 1.0.0

isEmpty(QT5BACKPORTS_LIBRARY_TYPE) {
    QT5BACKPORTS_LIBRARY_TYPE = shared
}

DEFINES += QT5BACKPORTS_LIBRARY

PUBLIC_HEADERS += \
    $${PWD}/qt5backports-exports.h

HEADERS *= \
    $${PUBLIC_HEADERS}

unix {
    PREFIX = /usr
} else {
    PREFIX = $$[QT_INSTALL_PREFIX]
}

LIBDIR = /lib
INCDIR = /include/qt5-backports

# install
target.path = $${PREFIX}$${LIBDIR}
headers.files = $${PUBLIC_HEADERS}
headers.path = $${PREFIX}$${INCDIR}
INSTALLS += target headers

# pkg-config support
CONFIG += create_pc create_prl no_install_prl
QMAKE_PKGCONFIG_DESTDIR = pkgconfig
QMAKE_PKGCONFIG_PREFIX = $${PREFIX}
QMAKE_PKGCONFIG_LIBDIR = $$target.path
QMAKE_PKGCONFIG_INCDIR = $$headers.path
equals(QT5BACKPORTS_LIBRARY_TYPE, staticlib) {
    QMAKE_PKGCONFIG_CFLAGS = -DQT5BACKPORTS_STATIC
    DEFINES += QT5BACKPORTS_STATIC
} else {
    QMAKE_PKGCONFIG_CFLAGS = -DQT5BACKPORTS_SHARED
    DEFINES += QT5BACKPORTS_SHARED
}
unix:QMAKE_CLEAN += -r pkgconfig lib$${TARGET}.prl
