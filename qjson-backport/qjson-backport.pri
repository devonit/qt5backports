INCLUDEPATH += $${PWD}
DEPENDPATH += $${PWD}

SOURCES += \
    $${PWD}/qjsonwriter.cpp \
    $${PWD}/qjsonvalue.cpp \
    $${PWD}/qjsonparser.cpp \
    $${PWD}/qjsonobject.cpp \
    $${PWD}/qjsondocument.cpp \
    $${PWD}/qjsonarray.cpp \
    $${PWD}/qjson.cpp

PRIVATE_HEADERS += \
    $${PWD}/qjsonwriter_p.h \
    $${PWD}/qjsonparser_p.h \
    $${PWD}/qjson_p.h

PUBLIC_HEADERS += \
    $${PWD}/qjsonvalue.h \
    $${PWD}/qjsonobject.h \
    $${PWD}/qjsondocument.h \
    $${PWD}/qjsonarray.h \
    $${PWD}/QJsonDocument

HEADERS *= \
    $${PUBLIC_HEADERS} \
    $${PRIVATE_HEADERS}
